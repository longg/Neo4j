﻿let app = new Vue({
    el: '#app',
    data: {
        svg:null,
        timer:null,
        editor:null,
        simulation:null,
        linkGroup:null,
        linkTextGroup:null,
        nodeGroup:null,
        nodeTextGroup:null,
        nodeSymbolGroup:null,
        nodeButtonGroup:null,
        nodeButtonAction:'',
        tooltip:null,
        tipsShow:true,
        txx:{},
        tyy:{},
        nodeDetail:null,
        pageSizeList: [{size: 100, isActive: true}, {size: 500, isActive: false}, {size: 1000,isActive: false}, {size: 2000, isActive: false}],
        colorList: ["#ff8373", "#f9c62c", "#a5ca34", "#6fce7a", "#70d3bd", "#ea91b0"],
        color5: '#ff4500',
        predefineColors: ['#ff4500', '#ff8c00', '#90ee90', '#00ced1', '#1e90ff', '#c71585'],
        defaultCr: 30,
        activeName: '',
        dataConfigActive: '',
        queryWords: '',
        operateType: 0,
        isEdit: false,
        isAddNode: false,
        isAddLink: false,
        isDeleteLink: false,
        isBatchCreate: false,
        selectNodeId: 0,
        selectNodeName: '',
        selectSourceNodeId: 0,
        selectTargetNodeId: 0,
        sourceNodeX1: 0,
        sourceNodeY1: 0,
        mouseX: 0,
        mouseY: 0,
        domain: '',
        domainId: 0,
        nodeName: '',
        pageSize: 100,
        cypherText:'',
        cypherTextShow:false,
        jsonShow:false,
        propActiveName: 'propEdit',
        contentActiveName: 'propImage',
        uploadImageUrl: contextRoot + "qiniu/upload",
        uploadImageParam: {},
        nodeImageList: [],
        netImageUrl: '',
        dialogImageVisible: false,
        dialogImageUrl: '',
        showImageList: [],
        editorContent: '',
        selectNode: {
            name: '',
            count: 0
        },
        pageModel: {
            pageIndex: 1,
            pageSize: 10,
            totalCount: 0,
            totalPage: 0,
            nodeList: []
        },
        recommendPageModel: {
            pageIndex: 1,
            pageSize: 10,
            totalCount: 0,
            totalPage: 0,
            nodeList: []
        },
        graph: {
            nodes: [],
            links: []
        },
        batchCreate: {
            sourceNodeName: '',
            targetNodeNames: '',
            relation: '',
        },
        graphNode: {
            uuid: 0,
            name: '',
            color: 'ff4500',
            r: 30,
            x: "",
            y: ""
        },
        uploadParam: {domain: "",type:0},
        uploadTips:{tips:"csv导入，注意字符集为utf-8无bom格式，三元组结构：节点-节点-关系",img:""},
        uploadTipsArr:[{tips:"csv导入，注意字符集为utf-8无bom格式，三元组结构：节点-节点-关系",name:"三元组",img:"",type:0},{tips:"支持合并单元格，设置颜色，设置关系需在节点后以###拼接，只识别一组关系",name:"单元格树",img:"http://file.miaoleyan.com/image-20211114183140256.png",type:1}],
        domainLabels: [],
        dialogFormVisible: false,
        exportFormVisible: false,
        headers: {},
        uploadUrl: contextRoot + "importGraph"
    },
    filters: {
        labelFormat: function (value) {
            let domain = value.substring(1, value.length - 1);
            return domain;
        },
    },
    mounted() {
        let token = $("meta[name='_csrf']").attr("content");
        let header = $("meta[name='_csrf_header']").attr("content");
        let str = '{ "' + header + '": "' + token + '"}';
        this.headers = eval('(' + str + ')');
        this.initGraph();
    },
    created() {
        this.getLabels();
        this.getRecommendLabels();
    },
    methods: {
        radioClick(m){
            debugger
            this.uploadTips=m;
        },
    	btnTipsClose(){
    		this.tipsShow=false;
    	},
        showCypher(){
            this.cypherTextShow=!this.cypherTextShow;
        },
        cypherJson(){
            if(this.graph.nodes.length==0&&this.graph.links.length==0){
                this.$message.error("请先选择领域或者执行cypher");
                return;
            }
            this.jsonShow=!this.jsonShow;
            let json=this.graph;
            let options={
                collapsed:false,//收缩所有节点
                withQuotes:false//为key添加双引号
            }
            $("#json-renderer").JSONView(json,options);
        },
        cypherRun(){
            let _this = this;
            if(_this.cypherText==""){
                _this.$message.error("请输入cypher语句");
                return;
            }
            let data = {cypher: _this.cypherText};
            $.ajax({
                data: data,
                type: "POST",
                url: contextRoot + "getCypherResult",
                success: function (result) {
                    if (result.code == 200) {
                        _this.graph.nodes = result.data.node;
                        _this.graph.links = result.data.relationship;
                        _this.updateGraph();
                    }else{
                        _this.$message.error(result.msg);
                    }
                }
            })
        },
        initEditor() {
            let  _this=this;
            if (_this.editor != null) return;
            let E = window.wangEditor;
            _this.editor = new E(this.$refs.editorToolbar, this.$refs.editorContent);
            _this.editor.customConfig.onchange = function (html) {
                _this.editorContent = html;
            };
            let token = $("meta[name='_csrf']").attr("content");
            let header = $("meta[name='_csrf_header']").attr("content");
            let str = '{ "' + header + '": "' + token + '"}';
            let headers = eval('(' + str + ')');
            _this.editor.customConfig.uploadFileName = 'file';
            _this.editor.customConfig.uploadImgHeaders = headers;
            _this.editor.customConfig.uploadImgServer = contextRoot + "qiniu/upload"; // 上传图片到服务器
            _this.editor.customConfig.uploadImgHooks = {
                // 如果服务器端返回的不是 {errno:0, data: [...]} 这种格式，可使用该配置
                // （但是，服务器端返回的必须是一个 JSON 格式字符串！！！否则会报错）
                customInsert: function (insertImg, res, editor) {
                    // 图片上传并返回结果，自定义插入图片的事件（而不是编辑器自动插入图片！！！）
                    // insertImg 是插入图片的函数，editor 是编辑器对象，result 是服务器端返回的结果
                    for (let i = 0; i < res.results.length; i++) {
                        let fileItem = res.results[i];
                        insertImg(fileItem.url)
                    }
                }
            }
            _this.editor.create();
        },
        initNodeContent() {
            let _this = this;
            let data = {domainId: _this.domainId, nodeId: _this.selectNodeId};
            $.ajax({
                data: data,
                type: "POST",
                url: contextRoot + "getNodeContent",
                success: function (result) {
                    _this.editor.txt.html("");
                    if (result.code == 200) {
                        _this.editorContent = result.data.Content;
                        editor.txt.html(result.data.Content)
                    }
                }
            })
        },
        initNodeImage() {
            let _this = this;
            let data = {domainId: _this.domainId, nodeId: _this.selectNodeId};
            $.ajax({
                data: data,
                type: "POST",
                url: contextRoot + "getNodeImage",
                success: function (result) {
                    if (result.code == 200) {
                        for (let i = 0; i < result.data.length; i++) {
                            _this.nodeImageList.push({
                                fileUrl: result.data[i].fileName,
                                imageType: result.data[i].ImageType
                            })
                        }
                    }
                }
            })
        },
        getNodeDetail(nodeId) {
            let _this = this;
            let data = {domainId: _this.domainId, nodeId: nodeId};
            $.ajax({
                data: data,
                type: "POST",
                url: contextRoot + "getNodeDetail",
                success: function (result) {
                    if (result.code == 200) {
                        _this.editorContent = result.data.content;
                        _this.showImageList = result.data.imageList;
                    }
                }
            })
        },
        saveNodeImage() {
            let _this = this;
            let data = {
                domainId: _this.domainId,
                nodeId: _this.selectNodeId,
                imageList: JSON.stringify(_this.nodeImageList)
            };
            $.ajax({
                dataType: 'json',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=UTF-8',
                type: "POST",
                url: contextRoot + "saveNodeImage",
                success: function (result) {
                    if (result.code == 200) {
                        _this.$message({
                            message: '操作成功',
                            type: 'success'
                        });
                    }
                }
            })
        },
        saveNodeContent() {
            let _this = this;
            let data = {domainId: _this.domainId, nodeId: _this.selectNodeId, content: _this.editorContent};
            $.ajax({
                dataType: 'json',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=UTF-8',
                type: "POST",
                url: contextRoot + "saveNodeContent",
                success: function (result) {
                    if (result.code == 200) {
                        _this.$message({
                            message: '操作成功',
                            type: 'success'
                        });
                    }
                }
            })
        },
        handlePictureCardPreview(item) {
            this.dialogImageUrl = this.imageUrlFormat(item);
            this.dialogImageVisible = true;
        },
        addNetImage() {
            if (this.netImageUrl != '') {
                this.nodeImageList.push({file: this.netImageUrl, imageType: 1});
                this.netImageUrl = '';
            }
        },
        imageHandleRemove(url) {
            this.nodeImageList.splice(this.nodeImageList.indexOf(url), 1);
        },
        imageUrlFormat(item) {
            return item.fileUrl;
        },
        dbImageUrlFormat(item) {
            return item.fileName;
        },
        uploadSuccess(res, file) {
            if (res.success == 1) {
                for (let i = 0; i < res.results.length; i++) {
                    let fileItem = res.results[i];
                    this.nodeImageList.push({fileUrl: fileItem.url});
                }

            } else {
                this.$message.error(res.msg);
            }
        },
        propHandleClick(tab, event) {
            if (tab.name == 'richTextEdit') {
                this.editorContent = '';
                this.initNodeContent();
                this.initEditor();

            }
            if (tab.name == 'propImage') {
                this.nodeImageList = [];
                this.initNodeImage();
            }
        },
        operateNameFormat() {
            if (this.operateType == 1) {
                return "添加同级";
            } else if (this.operateType == 2) {
                return "添加下级";
            } else if (this.operateType == 3) {
                return "批量添加";
            }
        },
        requestFullScreen() {
            let element = document.getElementById("graphcontainerdiv");
            let width = window.screen.width;
            let height = window.screen.height;
            this.svg.attr("width", width);
            this.svg.attr("height", height);
            if (element.requestFullscreen) {
                element.requestFullscreen();
            }
            // FireFox
            else if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            }
            // Chrome等
            else if (element.webkitRequestFullScreen) {
                element.webkitRequestFullScreen();
            }
            // IE11
            else if (element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }
        },
        getDomainGraph() {
            let _this = this;
            _this.loading = true;
            let data = {
                domain: _this.domain,
                nodeName: _this.nodeName,
                pageSize: _this.pageSize
            }
            $.ajax({
                data: data,
                type: "POST",
                url: contextRoot + "getDomainGraph",
                success: function (result) {
                    if (result.code == 200) {
                        let graphModel = result.data;
                        if (graphModel != null) {
                            _this.graph.nodes = graphModel.node;
                            _this.graph.links = graphModel.relationship;
                            _this.updateGraph();
                        }
                    }
                }
            });
        },
        getCurrentNodeInfo(node) {
            let _this = this;
            let data = {domain: _this.domain, nodeId: node.uuid};
            $.ajax({
                data: data,
                type: "POST",
                url: contextRoot + "getRelationNodeCount",
                success: function (result) {
                    if (result.code == 200) {
                        _this.selectNode.name = node.name;
                        _this.selectNode.count = result.data;
                    }
                }
            });
        },
        btnAddSingle(){
            d3.select('.graphContainer').style("cursor", "crosshair");//进入新增模式，鼠标变成＋
        },
        btnDeleteLink() {
            this.isDeleteLink = true;
            d3.select('.link').attr("class", "link linkDelete"); // 修改鼠标样式为"+"
        },
        getMoreNode() {
            let _this = this;
            let data = {domain: _this.domain, nodeId: _this.selectNodeId};
            $.ajax({
                data: data,
                type: "POST",
                url: contextRoot + "getMoreRelationNode",
                success: function (result) {
                    if (result.code == 200) {
                        let newNodes = result.data.node;
                        let newShips = result.data.relationship;
                        let oldNodesCount = _this.graph.nodes.length;
                        newNodes.forEach(function (m) {
                            let sobj = _this.graph.nodes.find(function (x) {
                                return x.uuid === m.uuid
                            })
                            if (typeof(sobj) == "undefined") {
                                _this.graph.nodes.push(m);
                            }
                        })
                        let newNodesCount = _this.graph.nodes.length;
                        if (newNodesCount <= oldNodesCount) {
                            _this.$message({
                                message: '没有更多节点信息',
                                type: 'success'
                            });
                            return;
                        }
                        newShips.forEach(function (m) {
                            let sobj = _this.graph.links.find(function (x) {
                                return x.uuid === m.uuid
                            })
                            if (typeof(sobj) == "undefined") {
                                _this.graph.links.push(m);
                            }
                        })
                        _this.updateGraph();
                    }
                },
                error: function (data) {
                }
            });
        },
        btnAddSame() {
            this.operateType = 1;
            this.isBatchCreate = true;
            this.isEdit = false;
        },
        btnQuickAddNode() {
            this.isEdit = false;
            this.isBatchCreate = true;
            $("#link_menubar").hide();
            this.operateType = 3;
        },
        deleteDomain(id,value) {
            let _this = this;
            _this.$confirm('此操作将删除该标签及其下节点和关系(不可恢复), 是否继续?', '三思而后行', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(function (res) {
                let data = {domainId:id,domain: value};
                $.ajax({
                    data: data,
                    type: "POST",
                    url: contextRoot + "deleteDomain",
                    success: function (result) {
                        if (result.code == 200) {
                            _this.getLabels();
                            _this.domain = "";
                        } else {
                            _this.$message({
                                showClose: true,
                                message: result.msg,
                                type: 'warning'
                            });
                        }
                    }
                });
            }).catch(function () {
                this.$message({
                    type: 'info',
                    message: '已取消删除'
                });
            });
        },
        createDomain(value) {
            let _this = this;
            _this.$prompt('请输入领域名称', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消'
            }).then(function (res) {
                value=res.value;
                let data = {domain: value};
                $.ajax({
                    data: data,
                    type: "POST",
                    url: contextRoot + "createDomain",
                    success: function (result) {
                        if (result.code == 200) {
                            _this.getLabels();
                            _this.domain = value;
                            _this.getDomainGraph();
                        } else {
                            _this.$message({
                                showClose: true,
                                message: result.msg,
                                type: 'warning'
                            });
                        }
                    }
                });
            }).catch(function () {
                this.$message({
                    type: 'info',
                    message: '取消输入'
                });
            });
        },
        getLabels() {
            let _this = this;
            let data = {};
            $.ajax({
                data: data,
                type: "POST",
                url: contextRoot + "getGraph",
                success: function (result) {
                    if (result.code == 200) {
                        _this.pageModel = result.data;
                        _this.pageModel.totalPage=parseInt((result.data.totalCount-1)/result.data.pageSize)+1
                    }
                }
            });
        },
        getRecommendLabels() {
            let _this = this;
            let data = {};
            $.ajax({
                data: data,
                type: "POST",
                url: contextRoot + "getRecommendGraph",
                success: function (result) {
                    if (result.code == 200) {
                        _this.recommendPageModel = result.data;
                        _this.recommendPageModel.totalPage=parseInt((result.data.totalCount-1)/result.data.pageSize)+1
                    }
                }
            });
        },
        getMoreDomain() {
            let _this = this;
            _this.pageModel.pageIndex=_this.pageModel.pageIndex+1
            let data = {pageIndex:_this.pageModel.pageIndex};
            $.ajax({
                data: data,
                type: "POST",
                url: contextRoot + "getGraph",
                success: function (result) {
                    if (result.code == 200) {
                    	_this.pageModel.nodeList.push.apply(_this.pageModel.nodeList,result.data.nodeList);
                    }
                }
            });
        },
        initGraph(){
            let graphContainer = d3.select(".graphContainer");
            let width = graphContainer._groups[0][0].offsetWidth;
            let height = window.screen.height - 154;//
            this.svg = graphContainer.append("svg");
            this.svg.attr("width", width);
            this.svg.attr("height", height);
            this.simulation = d3.forceSimulation()
                .force("link", d3.forceLink().distance(function(d){
                    return Math.floor(Math.random() * (700 - 200)) + 200;
                }).id(function (d) {
                    return d.uuid
                }))
                .force("charge", d3.forceManyBody().strength(-400))
                .force("collide", d3.forceCollide())
                .force("center", d3.forceCenter(width / 2, (height - 200) / 2));
            this.linkGroup = this.svg.append("g").attr("class", "line");
            this.linkTextGroup = this.svg.append("g").attr("class", "lineText");
            this.nodeGroup = this.svg.append("g").attr("class", "node");
            this.nodeTextGroup = this.svg.append("g").attr("class", "nodeText");
            this.nodeSymbolGroup = this.svg.append("g").attr("class", "nodeSymbol");
            this.nodeButtonGroup = this.svg.append("g").attr("class", "nodeButton");
            this.addMaker();
            this.tooltip =  this.svg.append("div").style("opacity", 0);
            this.svg.on('click',function(){
                d3.selectAll(".buttongroup").classed("circle_operate", true);
            }, 'false');

        },
        updateGraph() {
            let _this = this;
            let lks = this.graph.links;
            let nodes = this.graph.nodes;
            let links = [];
            //由后端传过来的节点坐标，固定节点，由于是字符串，需要转换
            nodes.forEach(function (n) {
                if(typeof (n.fx)=="undefined"||n.fx==""||n.fx==null){
                    n.fx=null;
                }
                if(typeof (n.fy)=="undefined"||n.fy==""||n.fy==null){
                    n.fy=null;
                }
                if((typeof n.fx) == "string") n.fx = parseFloat(n.fx);
                if((typeof n.fy) == "string") n.fy = parseFloat(n.fy);
            });
            lks.forEach(function (m) {
                let sourceNode = nodes.filter(function (n) {
                    return n.uuid === m.sourceId;
                })[0];
                if (typeof(sourceNode) == 'undefined') return;
                let targetNode = nodes.filter(function (n) {
                    return n.uuid === m.targetId;
                })[0];
                if (typeof(targetNode) == 'undefined') return;
                links.push({source: sourceNode.uuid, target: targetNode.uuid, lk: m});
            });
            //为每一个节点定制按钮组
            _this.addNodeButton();
           if(links.length>0){
               _.each(links, function(link) {
                   let same = _.where(links, {
                       'source': link.source,
                       'target': link.target
                   });
                   let sameAlt = _.where(links, {
                       'source': link.target,
                       'target': link.source
                   });
                   let sameAll = same.concat(sameAlt);
                   _.each(sameAll, function(s, i) {
                       s.sameIndex = (i + 1);
                       s.sameTotal = sameAll.length;
                       s.sameTotalHalf = (s.sameTotal / 2);
                       s.sameUneven = ((s.sameTotal % 2) !== 0);
                       s.sameMiddleLink = ((s.sameUneven === true) &&(Math.ceil(s.sameTotalHalf) === s.sameIndex));
                       s.sameLowerHalf = (s.sameIndex <= s.sameTotalHalf);
                       s.sameArcDirection = 1;
                       //s.sameArcDirection = s.sameLowerHalf ? 0 : 1;
                       s.sameIndexCorrected = s.sameLowerHalf ? s.sameIndex : (s.sameIndex - Math.ceil(s.sameTotalHalf));
                   });
               });
               let maxSame = _.chain(links)
                   .sortBy(function(x) {
                       return x.sameTotal;
                   })
                   .last()
                   .value().sameTotal;

               _.each(links, function(link) {
                   link.maxSameHalf = Math.round(maxSame / 2);
               });
           }
            // 更新连线 links
            let link = _this.linkGroup.selectAll(".line >path").data(links, function (d) {
                return d.uuid;
            });
            link.exit().remove();
            let linkEnter = _this.drawLink(link);
            link = linkEnter.merge(link);
            // 更新连线文字
            _this.linkTextGroup.selectAll("text").data(links, function (d) {
                return d.uuid;
            }).exit().remove();//移除多余的text dom
            let linktext = _this.linkTextGroup.selectAll("text >textPath").data(links, function (d) {
                return d.uuid;
            });
            linktext.exit().remove();
            let linkTextEnter = _this.drawLinkText(linktext);
            linktext = linkTextEnter.merge(linktext).text(function (d) {
                return d.lk.name;
            });
            // 更新节点按钮组
            d3.selectAll(".nodeButton >g").remove();
            let nodeButton = _this.nodeButtonGroup.selectAll(".nodeButton").data(nodes, function (d) {
                return d
            });
            nodeButton.exit().remove();
            let nodeButtonEnter = _this.drawNodeButton(nodeButton);
            nodeButton = nodeButtonEnter.merge(nodeButton);
            // 更新节点
            let node = _this.nodeGroup.selectAll("circle").data(nodes, function (d) {
                return d.uuid;
            });
            node.exit().remove();
            let nodeEnter = _this.drawNode(node);
            node = nodeEnter.merge(node).text(function (d) {
                return d.name;
            });
            // 更新节点文字
            let nodeText = _this.nodeTextGroup.selectAll("text").data(nodes, function (d) {
                return d.uuid
            });
            nodeText.exit().remove();
            let nodeTextEnter = _this.drawNodeText(nodeText);
            nodeText = nodeTextEnter.merge(nodeText).text(function (d) {
                return d.name;
            });
            nodeText.append("title")// 为每个节点设置title
                .text(function (d) {
                    return d.name;
                });
            // 更新节点标识
            let nodeSymbol = _this.nodeSymbolGroup.selectAll("path").data(nodes, function (d) {
                return d.uuid;
            });
            nodeSymbol.exit().remove();
            let nodeSymbolEnter = _this.drawNodeSymbol(nodeSymbol);
            nodeSymbol = nodeSymbolEnter.merge(nodeSymbol);
            nodeSymbol.attr("fill", "#e15500");
            nodeSymbol.attr("display", function (d) {
                if (typeof(d.hasFile) != "undefined" && d.hasFile > 0) {
                    return "block";
                }
                return "none";
            })
            _this.simulation.nodes(nodes).on("tick", ticked);
            _this.simulation.force("link").links(links);
            _this.simulation.alphaTarget(1).restart();
            function linkArc(d) {
                let dx = (d.target.x - d.source.x),
                    dy = (d.target.y - d.source.y),
                    dr = Math.sqrt(dx * dx + dy * dy),
                    unevenCorrection = (d.sameUneven ? 0 : 0.5);
                let curvature = 2,
                    arc = (1.0/curvature)*((dr * d.maxSameHalf) / (d.sameIndexCorrected - unevenCorrection));
                if (d.sameMiddleLink) {
                    arc = 0;
                }
                let dd="M" + d.source.x + "," + d.source.y + "A" + arc + "," + arc + " 0 0," + d.sameArcDirection + " " + d.target.x + "," + d.target.y;
                return dd;
            }

            function ticked() {
                link.attr("d", linkArc)
                // 更新节点坐标
                node.attr("cx", function (d) {
                    return d.x;
                })
                    .attr("cy", function (d) {
                        return d.y;
                    });
                // 更新节点操作按钮组坐标
                nodeButton.attr("cx", function (d) {
                    return d.x;
                })
                    .attr("cy", function (d) {
                        return d.y;
                    });
               nodeButton.attr("transform", function (d) {
                    return "translate(" + d.x + "," + d.y+ ") scale(1)";
                })

                // 更新文字坐标
                nodeText.attr("x", function (d) {
                    return d.x;
                })
                    .attr("y", function (d) {
                        return d.y;
                    });
                // 更新回形针坐标
                nodeSymbol.attr("transform", function (d) {
                    return "translate(" + (d.x + 8) + "," + (d.y - 30) + ") scale(0.015,0.015)";
                })
            }
            // 鼠标滚轮缩放
            //_this.svg.call(d3.zoom().transform, d3.zoomIdentity);//缩放至初始倍数
            _this.svg.call(d3.zoom().on("zoom", function () {
            	d3.select('#link_menubar').style('display', 'none');
            	d3.select('#nodeDetail').style('display', 'none');
                d3.selectAll('.node').attr("transform",d3.event.transform);
                d3.selectAll('.nodeText').attr("transform",d3.event.transform);
                d3.selectAll('.line').attr("transform",d3.event.transform);
                d3.selectAll('.lineText').attr("transform",d3.event.transform);
                d3.selectAll('.nodeSymbol').attr("transform",d3.event.transform);
                d3.selectAll('.nodeButton').attr("transform",d3.event.transform);
                //_this.svg.selectAll("g").attr("transform", d3.event.transform);
            }));
            _this.svg.on("dblclick.zoom", null); // 静止双击缩放
            //按钮组事件
            _this.svg.selectAll(".buttongroup").on("click", function (d,i) {
                if (_this.nodeButtonAction) {
                    switch (_this.nodeButtonAction) {
                        case "EDIT":
                            _this.isEdit = true;
                            _this.propActiveName = 'propEdit';
                            _this.txx=d.x;
                            _this.tyy=d.y;
                            break;
                        case "MORE":
                            _this.getMoreNode();
                            break;
                        case "CHILD":
                            _this.operateType = 2;
                            _this.isBatchCreate = true;
                            _this.isEdit = false;
                            break;
                        case "LINK":
                            _this.isAddLink = true;
                            _this.selectSourceNodeId=d.uuid;
                            break;
                        case "DELETE":
                            _this.selectNodeId=d.uuid;
                            let out_buttongroup_id='.out_buttongroup_'+i;
                            _this.deleteNode(out_buttongroup_id);
                            break;
                    }
                    //ACTION = '';//重置 ACTION
                }

            });
            //按钮组事件绑定
            _this.svg.selectAll(".action_0").on("click", function (d) {
                _this.nodeButtonAction='EDIT';
            });
            _this.svg.selectAll(".action_1").on("click", function (d) {
                _this.nodeButtonAction='MORE';
            });
            _this.svg.selectAll(".action_2").on("click", function (d) {
                _this.nodeButtonAction='CHILD';
            });
            _this.svg.selectAll(".action_3").on("click", function (d) {
                _this.nodeButtonAction='LINK';
            });
            _this.svg.selectAll(".action_4").on("click", function (d) {
                _this.nodeButtonAction='DELETE';
            });
        },
        createNode() {
            let _this = this;
            let data = _this.graphNode;
            data.domain = _this.domain;
            $.ajax({
                data: data,
                type: "POST",
                traditional: true,
                url: contextRoot + "createNode",
                success: function (result) {
                    if (result.code == 200) {
                        if (_this.graphNode.uuid != 0) {
                            for (let i = 0; i < _this.graph.nodes.length; i++) {
                                if (_this.graph.nodes[i].uuid == _this.graphNode.uuid) {
                                    _this.graph.nodes.splice(i, 1);
                                }
                            }
                        }
                        let newNode = result.data;
                        newNode.x = _this.txx;
                        newNode.y = _this.tyy;
                        newNode.fx = _this.txx;
                        newNode.fy = _this.tyy;
                        _this.graph.nodes.push(newNode);
                        _this.resetNode();
                        _this.updateGraph();
                        _this.isEdit = false;
                        _this.resetSubmit();
                    }
                }
            });
        },
        createSingleNode() {
            let _this = this;
            let data = {name:'',r:30};
            data.domain = _this.domain;
            $.ajax({
                data: data,
                type: "POST",
                traditional: true,
                url: contextRoot + "createNode",
                success: function (result) {
                    if (result.code == 200) {
                        d3.select('.graphContainer').style("cursor", "");
                        let newNode = result.data;
                        newNode.x = _this.txx;
                        newNode.y = _this.tyy;
                        newNode.fx = _this.txx;
                        newNode.fy = _this.tyy;
                        _this.graph.nodes.push(newNode);
                        _this.updateGraph();
                    }
                }
            });
        },
        addMaker() {
            let arrowMarker = this.svg.append("marker")
                .attr("id", "arrow")
                .attr("markerUnits", "strokeWidth")
                .attr("markerWidth", "20")//
                .attr("markerHeight", "20")
                .attr("viewBox", "0 -5 10 10")
                .attr("refX", "22")// 13
                .attr("refY", "0")
                .attr("orient", "auto");
            let arrow_path = "M0,-5L10,0L0,5";// 定义箭头形状
            arrowMarker.append("path").attr("d", arrow_path).attr("fill", "#fce6d4");
        },
        addNodeButton(r) {
            //先删除所有为节点自定义的按钮组
            d3.selectAll("svg >defs").remove();
            let nodes = this.graph.nodes;
            let database = [1,1,1,1,1];
            let pie = d3.pie();
            let piedata = pie(database);
            let nodeButton = this.svg.append("defs");
            nodes.forEach(function(m){
                let nBtng=nodeButton.append("g")
                    .attr("id", "out_circle"+m.uuid);//为每一个节点定制一个按钮组，在画按钮组的时候为其指定该id
                let buttonEnter=nBtng.selectAll(".buttongroup")
                    .data(piedata)
                    .enter()
                    .append("g")
                    .attr("class", function (d, i) {
                        return "action_" + i ;
                    });
                let defaultR=30;
                if(typeof (m.r)=='undefined'){
                    m.r=defaultR;
                }
                let arc = d3.arc()
                    .innerRadius(m.r)
                    .outerRadius(m.r+30);
                buttonEnter.append("path")
                    .attr("d", function (d) {
                        return arc(d)
                    })
                    .attr("fill", "#D2D5DA")
                    .style("opacity", 0.6)
                    .attr("stroke", "#f0f0f4")
                    .attr("stroke-width", 2);
                buttonEnter.append("text")
                    .attr("transform", function (d, i) {
                        return "translate(" + arc.centroid(d) + ")";
                    })
                    .attr("text-anchor", "middle")
                    .text(function (d, i) {
                        let zi = new Array()
                        zi[0] = "编辑";
                        zi[1] = "展开";
                        zi[2] = "追加";
                        zi[3] = "连线";
                        zi[4] = "删除";
                        return zi[i]
                    })
                    .attr("font-size", 10);
            })
        },
        dragStarted(d) {
            if (!d3.event.active) this.simulation.alphaTarget(0.3).restart();
            d.fx = d.x;
            d.fy = d.y;
            d.fixed = true;
        },
        dragged(d) {
            d.fx = d3.event.x;
            d.fy = d3.event.y;
        },
        dragEnded(d) {
            if (!d3.event.active) this.simulation.alphaTarget(0);
            d.fx = d3.event.x;
            d.fy = d3.event.y;
            let domain = this.domain;
            let uuid = d.uuid;
            let fx = d.fx;
            let fy = d.fy;
            let ajaxdata = {domain:domain,uuid:uuid,fx:fx,fy:fy};
            $.ajax({
                data: ajaxdata,
                type: "POST",
                url: contextRoot+"updateCoordinateOfNode",
                success: function (result) {
                    if (result.code == 200) {
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown)
                }
            });
        },
        drawNode(node) {
            let _this = this;
            let nodeEnter = node.enter().append("circle");
            nodeEnter.attr("r", function (d) {
                if (typeof(d.r) != "undefined" && d.r != '') {
                    return d.r
                }
                return 30;
            });
            nodeEnter.attr("fill", function (d) {
                if (typeof(d.color) != "undefined" && d.color != '') {
                    return d.color
                }
                return "#ff4500";
            });
            //nodeEnter.style("opacity", 0.8);
            nodeEnter.style("opacity", 1);
            nodeEnter.style("stroke", function (d) {
                if (typeof(d.color) != "undefined" && d.color != '') {
                    return d.color
                }
                return "#ff4500";
            });
            nodeEnter.style("stroke-opacity", 0.6);
            nodeEnter.append("title")// 为每个节点设置title
                .text(function (d) {
                    return d.name;
                })
            nodeEnter.on("mouseover", function (d, i) {
            	 _this.nodeDetail=d;
                 _this.timer = setTimeout(function () {
                    d3.select('#richContainer')
                        .style('position', 'absolute')
                        .style('left', d.x + "px")
                        .style('top', d.y + "px")
                        .style('display', 'block');
                    _this.editorContent = "";
                    _this.showImageList = [];
                    _this.getNodeDetail(d.uuid);
                }, 2000);
            });
            nodeEnter.on("mouseout", function (d, i) {
                clearTimeout( _this.timer);
            });
            nodeEnter.on("dblclick", function (d) {
                app.updateNodeName(d);// 双击更新节点名称
            });
            nodeEnter.on("mouseenter", function (d) {
                let aa = d3.select(this)._groups[0][0];
                if (aa.classList.contains("selected")) return;
                d3.select(this).style("stroke-width", "6");
            });
            nodeEnter.on("mouseleave", function (d) {
                let aa = d3.select(this)._groups[0][0];
                if (aa.classList.contains("selected")) return;
                d3.select(this).style("stroke-width", "2");
            });
            nodeEnter.on("click", function (d,i) {
                d3.select('#nodeDetail').style('display', 'block');
                let out_buttongroup_id='.out_buttongroup_'+i;
                _this.svg.selectAll(".buttongroup").classed("circle_operate", true);
                _this.svg.selectAll(out_buttongroup_id).classed("circle_operate", false);
                _this.graphNode = d;
                _this.selectNodeId = d.uuid;
                _this.selectNodeName = d.name;
                // 添加连线状态
                if (_this.isAddLink) {
                    _this.selectTargetNodeId = d.uuid;
                    if (_this.selectSourceNodeId == _this.selectTargetNodeId || _this.selectSourceNodeId == 0 || _this.selectTargetNodeId == 0) return;
                    _this.createLink(_this.selectSourceNodeId, _this.selectTargetNodeId, "RE")
                    _this.selectSourceNodeId = 0;
                    _this.selectTargetNodeId = 0;
                    d.fixed = false
                    d3.event.stopPropagation();
                }
            });
            nodeEnter.call(d3.drag()
                .on("start", _this.dragStarted)
                .on("drag", _this.dragged)
                .on("end", _this.dragEnded));
            return nodeEnter;
        },
        drawNodeText(nodeText) {
            let _this = this;
            let nodeTextEnter = nodeText.enter().append("text")
                .style("fill", "#fff")
                .attr("dy", 4)
                .attr("font-family", "微软雅黑")
                .attr("text-anchor", "middle")
                .text(function (d) {
                	if(typeof(d.name)=='undefined') return '';
                    let length = d.name.length;
                    if (d.name.length > 4) {
                        let s = d.name.slice(0, 4) + "...";
                        return s;
                    }
                    return d.name;
                });
            nodeTextEnter.on("mouseover", function (d, i) {
                _this.timer = setTimeout(function () {
                    d3.select('#richContainer')
                        .style('position', 'absolute')
                        .style('left', d.x + "px")
                        .style('top', d.y + "px")
                        .style('display', 'block');
                    _this.editorContent = "";
                    _this.showImageList = [];
                    _this.getNodeDetail(d.uuid);
                }, 3000);
            });

            nodeTextEnter.on("dblclick", function (d) {
                app.updateNodeName(d);// 双击更新节点名称
            });
            nodeTextEnter.on("click", function (d) {
                $('#link_menubar').hide();// 隐藏空白处右键菜单
                _this.graphNode = d;
                _this.selectNodeId = d.uuid;
                // 更新工具栏节点信息
                _this.getCurrentNodeInfo(d);
                // 添加连线状态
                if (_this.isAddLink) {
                    _this.selectTargetNodeId = d.uuid;
                    if (_this.selectSourceNodeId == _this.selectTargetNodeId || _this.selectSourceNodeId == 0 || _this.selectTargetNodeId == 0) return;
                    _this.createLink(_this.selectSourceNodeId, _this.selectTargetNodeId, "RE")
                    _this.selectSourceNodeId = 0;
                    _this.selectTargetNodeId = 0;
                    d.fixed = false
                    d3.event.stopPropagation();
                }
            });

            return nodeTextEnter;
        },
        drawNodeSymbol(nodeSymbol) {
            let _this = this;
            let symbol_path = "M566.92736 550.580907c30.907733-34.655573 25.862827-82.445653 25.862827-104.239787 0-108.086613-87.620267-195.805867-195.577173-195.805867-49.015467 0-93.310293 18.752853-127.68256 48.564907l-0.518827-0.484693-4.980053 4.97664c-1.744213 1.64864-3.91168 2.942293-5.59104 4.72064l0.515413 0.484693-134.69696 133.727573L216.439467 534.8352l0 0 137.478827-136.31488c11.605333-10.410667 26.514773-17.298773 43.165013-17.298773 36.051627 0 65.184427 29.197653 65.184427 65.24928 0 14.032213-5.33504 26.125653-12.73856 36.829867l-131.754667 132.594347 0.515413 0.518827c-10.31168 11.578027-17.07008 26.381653-17.07008 43.066027 0 36.082347 29.16352 65.245867 65.184427 65.245867 16.684373 0 31.460693-6.724267 43.035307-17.07008l0.515413 0.512M1010.336427 343.49056c0-180.25472-145.882453-326.331733-325.911893-326.331733-80.704853 0-153.77408 30.22848-210.418347 79.0528l0.484693 0.64512c-12.352853 11.834027-20.241067 28.388693-20.241067 46.916267 0 36.051627 29.16352 65.245867 65.211733 65.245867 15.909547 0 29.876907-6.36928 41.192107-15.844693l0.38912 0.259413c33.624747-28.030293 76.301653-45.58848 123.511467-45.58848 107.99104 0 195.549867 87.6544 195.549867 195.744427 0 59.815253-27.357867 112.71168-69.51936 148.503893l0 0-319.25248 317.928107 0 0c-35.826347 42.2912-88.654507 69.710507-148.340053 69.710507-107.956907 0-195.549867-87.68512-195.549867-195.805867 0-59.753813 27.385173-112.646827 69.515947-148.43904l-92.18048-92.310187c-65.69984 59.559253-107.700907 144.913067-107.700907 240.749227 0 180.28544 145.885867 326.301013 325.915307 326.301013 95.218347 0 180.02944-41.642667 239.581867-106.827093l0.13312 0.129707 321.061547-319.962453-0.126293-0.13312C968.69376 523.615573 1010.336427 438.71232 1010.336427 343.49056L1010.336427 343.49056 1010.336427 343.49056zM1010.336427 343.49056";// 定义回形针形状
            let nodeSymbolEnter = nodeSymbol.enter().append("path").attr("d", symbol_path);
            nodeSymbolEnter.call(d3.drag()
                .on("start", _this.dragStarted)
                .on("drag", _this.dragged)
                .on("end", _this.dragEnded));
            return nodeSymbolEnter;
        },
        drawNodeButton(nodeButton) {
            let _this = this;
            let nodeButtonEnter = nodeButton.enter().append("g").append("use")//  为每个节点组添加一个 use 子元素
                .attr("r", function(d){
                    return d.r;
                })
                .attr("xlink:href", function (d) {
                    return "#out_circle"+d.uuid;
                }) //  指定 use 引用的内容
                .attr('class',function(d,i){
                    return 'buttongroup out_buttongroup_'+i;
                })
                .classed("circle_operate", true);

            return nodeButtonEnter;
        },
        drawLink(link) {
            let _this = this;
            let linkEnter = link.enter().append("path")
                .attr("stroke-width", 1)
                .attr("stroke", "#fce6d4")
                .attr("fill", "none")
                .attr("id", function (d) {
                    return "invis_" + d.lk.sourceId + "-" + d.lk.name + "-" + d.lk.targetId;
                })
                .attr("marker-end", "url(#arrow)");// 箭头
            linkEnter.on("dblclick", function (d) {
                _this.selectNodeId = d.lk.uuid;
                if (_this.isDeleteLink) {
                    _this.deleteLink();
                } else {
                    _this.updateLinkName();
                }
            });
            linkEnter.on("contextmenu", function (d) {
                let cc = $(this).offset();
                app.selectNodeId = d.lk.uuid;
                app.selectlinkname = d.lk.name;
                d3.select('#link_menubar')
                    .style('position', 'absolute')
                    .style('left', cc.left + "px")
                    .style('top', cc.top + "px")
                    .style('display', 'block');
                d3.event.preventDefault();// 禁止系统默认右键
                d3.event.stopPropagation();// 禁止空白处右键
            });
            linkEnter.on("mouseenter", function (d) {
                d3.select(this).style("stroke-width", "6").attr("stroke", "#ff9e9e").attr("marker-end", "url(#arrow)");
                _this.nodeDetail=d.lk;
                d3.select('#nodeDetail').style('display', 'block');
            });
            linkEnter.on("mouseleave", function (d) {
                d3.select(this).style("stroke-width", "1").attr("stroke", "#fce6d4").attr("marker-end", "url(#arrow)");
            });
            return linkEnter;
        },
        drawLinkText(link) {
            let linkTextEnter = link.enter().append('text')
                .style('fill', '#e3af85')
                .append("textPath")
                .attr("startOffset", "50%")
                .attr("text-anchor", "middle")
                .attr("xlink:href", function(d) {
                    return "#invis_" + d.lk.sourceId + "-" + d.lk.name + "-" + d.lk.targetId;
                })
                .style("font-size", 14)
                .text(function (d) {
                    if (d.lk.name != '') {
                        return d.lk.name;
                    }
                });

            linkTextEnter.on("mouseover", function (d) {
                app.selectNodeId = d.lk.uuid;
                app.selectlinkname = d.lk.name;
                let cc = $(this).offset();
                d3.select('#link_menubar')
                    .style('position', 'absolute')
                    .style('left', cc.left + "px")
                    .style('top', cc.top + "px")
                    .style('display', 'block');
            });

            return linkTextEnter;
        },
        deleteNode(out_buttongroup_id) {
            let _this = this;
            _this.$confirm('此操作将删除该节点及周边关系(不可恢复), 是否继续?', '三思而后行', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(function () {
                let data = {domain: _this.domain, nodeId: _this.selectNodeId};
                $.ajax({
                    data: data,
                    type: "POST",
                    url: contextRoot + "deleteNode",
                    success: function (result) {
                        if (result.code == 200) {
                            _this.svg.selectAll(out_buttongroup_id).remove();
                            let rShips = result.data;
                            // 删除节点对应的关系
                            for (let m = 0; m < rShips.length; m++) {
                                for (let i = 0; i < _this.graph.links.length; i++) {
                                    if (_this.graph.links[i].uuid == rShips[m].uuid) {
                                        _this.graph.links.splice(i, 1);
                                        i = i - 1;
                                    }
                                }
                            }
                            // 找到对应的节点索引
                            let j = -1;
                            for (let i = 0; i < _this.graph.nodes.length; i++) {
                                if (_this.graph.nodes[i].uuid == _this.selectNodeId) {
                                    j = i;
                                    break;
                                }
                            }
                            if (j >= 0) {
                                _this.selectNodeId = 0;
                                _this.graph.nodes.splice(i, 1);// 根据索引删除该节点
                                _this.updateGraph();
                                _this.resetNode();
                                _this.$message({
                                    type: 'success',
                                    message: '操作成功!'
                                });
                            }

                        }
                    }
                })
            }).catch(function () {
                _this.$message({
                    type: 'info',
                    message: '已取消删除'
                });
            });
        },
        deleteLink() {
            let _this = this;
            this.$confirm('此操作将删除该关系(不可恢复), 是否继续?', '三思而后行', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(function () {
                let data = {domain: _this.domain, shipId: _this.selectNodeId};
                $.ajax({
                    data: data,
                    type: "POST",
                    url: contextRoot + "deleteLink",
                    success: function (result) {
                        if (result.code == 200) {
                            let j = -1;
                            for (let i = 0; i < _this.graph.links.length; i++) {
                                if (_this.graph.links[i].uuid == _this.selectNodeId) {
                                    j = i;
                                    break;
                                }
                            }
                            if (j >= 0) {
                                _this.selectNodeId = 0;
                                _this.graph.links.splice(i, 1);
                                _this.updateGraph();
                                _this.isDeleteLink = false;
                            }
                        }
                    }
                });
            }).catch(function () {
                this.$message({
                    type: 'info',
                    message: '已取消删除'
                });
            });
        },
        createLink(sourceId, targetId, ship) {
            let _this = this;
            let data = {domain: _this.domain, sourceId: sourceId, targetId: targetId, ship: ''};
            $.ajax({
                data: data,
                type: "POST",
                url: contextRoot + "createLink",
                success: function (result) {
                    if (result.code == 200) {
                        let newShip = result.data;
                        _this.graph.links.push(newShip);
                        _this.updateGraph();
                        _this.isAddLink = false;
                    }
                }
            });
        },
        updateLinkName() {
            let _this = this;
            _this.$prompt('请输入关系名称', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                inputValue: this.selectlinkname
            }).then(function (res) {
                let value=res.value;
                let data = {domain: _this.domain, shipId: _this.selectNodeId, shipName: value};
                $.ajax({
                    data: data,
                    type: "POST",
                    url: contextRoot + "updateLink",
                    success: function (result) {
                        if (result.code == 200) {
                            let newShip = result.data;
                            _this.graph.links.forEach(function (m) {
                                if (m.uuid == newShip.uuid) {
                                    m.name = newShip.name;
                                }
                            });
                            _this.selectNodeId = 0;
                            _this.updateGraph();
                            _this.isAddLink = false;
                            _this.selectlinkname = '';
                        }
                    }
                });
            }).catch(function () {
                _this.$message({
                    type: 'info',
                    message: '取消输入'
                });
            });
        },
        updateNodeName(d) {
            let _this = this;
            _this.$prompt('编辑节点名称', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                inputValue: d.name
            }).then(function (res) {
                let value=res.value;
                let data = {domain: _this.domain, nodeId: d.uuid, nodeName: value};
                $.ajax({
                    data: data,
                    type: "POST",
                    url: contextRoot + "updateNodeName",
                    success: function (result) {
                        if (result.code == 200) {
                            if (d.uuid != 0) {
                                for (let i = 0; i < _this.graph.nodes.length; i++) {
                                    if (_this.graph.nodes[i].uuid == d.uuid) {
                                        _this.graph.nodes[i].name = value;
                                    }
                                }
                            }
                            _this.updateGraph();
                            _this.$message({
                                message: '操作成功',
                                type: 'success'
                            });
                        }
                    }
                });
            }).catch(function () {
                _this.$message({
                    type: 'info',
                    message: '取消操作'
                });
            });
        },
        resetSubmit() {
            this.isAddNode = false;
            this.isEdit = false;
            this.resetNode();
            this.fieldDataList = [];
            this.dataConfigActive = '';
            this.isBatchCreate = false;
            this.selectNodeId = 0;
        },
        resetNode() {
            this.graphNode = {
                uuid: 0,
                color: 'ff4500',
                name: '',
                r: 30,
                x: '',
                y: ''
            };
        },
        initForm(d) {
            this.graphNode = {
                uuid: d.uuid,
                name: d.name,
                color: d.color,
                r: d.r,
                x: d.x,
                y: d.y
            };
        },
        matchDomainGraph(domain, event) {
            this.domain = domain.name;
            this.domainId = domain.id;
            this.getDomainGraph()
        },
        refreshNode(event) {
            $(".ml-a").removeClass("cur");
            $(event.currentTarget).addClass("cur");
            this.nodeName = '';
            this.getDomainGraph();
        },
        getFilterDomain(domainList) {
            let array = [];
            for (let i = 0; i < domainList.length; i++) {
                array.push({value: domainList[i].label.substring(1, domainList[i].label.length - 1)});
            }
            return array;
        },
        operateCommand(command) {
            if (command === 'image') {
                this.exportImage();
            }
            if (command === 'import') {
                this.dialogFormVisible = true;
            }
            if (command === 'export') {
                this.exportFormVisible = true;
            }
        },
        exportCsv() {
            let _this = this;
            $.ajax({
                data: {domain: _this.uploadParam.domain},
                type: "POST",
                url: contextRoot + "exportGraph",
                success: function (result) {
                    if (result.code == 200) {
                        _this.exportFormVisible = false;
                        window.open(result.csvUrl);
                    }
                }
            });
        },
        submitUpload() {
            this.$refs.upload.submit();
            this.dialogFormVisible = false;
        },
        csvSuccess() {
            this.$refs.upload.clearFiles();
            this.uploadParam.domain = "";
            this.$message({
                message: "正在导入中,请稍后查看",
                type: 'success'
            });
        },
        exportImage() {
            /*https://html2canvas.hertzen.com/getting-started  截图js*/
            html2canvas(document.querySelector(".graphContainer")).then(function (canvas) {
                let a = document.createElement('a');
                a.href = canvas.toDataURL('image/png');  //将画布内的信息导出为png图片数据
                let timeStamp = Date.parse(new Date());
                a.download = timeStamp;  //设定下载名称
                a.click(); //点击触发下载
            });
        },
        setMatchSize(m, event) {
            for (let i = 0; i < this.pageSizeList.length; i++) {
                this.pageSizeList[i].isActive = false;
                if (this.pageSizeList[i].size == m.size) {
                    this.pageSizeList[i].isActive = true;
                }
            }
            this.pageSize = m.size;
            this.getDomainGraph();
        },
        batchCreateNode() {
            let _this = this;
            let data = {
                domain: _this.domain,
                sourceName: _this.batchCreate.sourceNodeName,
                targetNames: _this.batchCreate.targetNodeNames,
                relation: _this.batchCreate.relation
            };
            $.ajax({
                data: data,
                type: "POST",
                url: contextRoot + "batchCreateNode",
                success: function (result) {
                    if (result.code == 200) {
                        _this.isBatchCreate = false;
                        let newNodes = result.data.nodes;
                        let newShips = result.data.ships;
                        newNodes.forEach(function (m) {
                            let sobj = _this.graph.nodes.find(function (x) {
                                return x.uuid === m.uuid
                            })
                            if (typeof(sobj) == "undefined") {
                                _this.graph.nodes.push(m);
                            }
                        })
                        newShips.forEach(function (m) {
                            let sobj = _this.graph.links.find(function (x) {
                                return x.uuid === m.uuid
                            })
                            if (typeof(sobj) == "undefined") {
                                _this.graph.links.push(m);
                            }
                        })
                        _this.updateGraph();
                        _this.batchCreate.sourceNodeName = '';
                        _this.batchCreate.targetNodeNames = '';
                        _this.$message({
                            message: '操作成功',
                            type: 'success'
                        });
                    }
                }
            });
        },
        batchCreateChildNode() {
            let _this = this;
            let data = {
                domain: _this.domain,
                sourceId: _this.selectNodeId,
                targetNames: _this.batchCreate.targetNodeNames,
                relation: _this.batchCreate.relation
            };
            $.ajax({
                data: data,
                type: "POST",
                url: contextRoot + "batchCreateChildNode",
                success: function (result) {
                    if (result.code == 200) {
                        _this.isBatchCreate = false;
                        let newNodes = result.data.nodes;
                        let newShips = result.data.ships;
                        newNodes.forEach(function (m) {
                            let sobj = _this.graph.nodes.find(function (x) {
                                return x.uuid === m.uuid
                            })
                            if (typeof(sobj) == "undefined") {
                                _this.graph.nodes.push(m);
                            }
                        })
                        newShips.forEach(function (m) {
                            let sobj = _this.graph.links.find(function (x) {
                                return x.uuid === m.uuid
                            })
                            if (typeof(sobj) == "undefined") {
                                _this.graph.links.push(m);
                            }
                        })
                        _this.updateGraph();
                        _this.batchCreate.sourceNodeName = '';
                        _this.batchCreate.targetNodeNames = '';
                        _this.$message({
                            message: '操作成功',
                            type: 'success'
                        });
                    }
                }
            });
        },
        batchCreateSameNode() {
            let _this = this;
            let data = {domain: _this.domain, sourceNames: _this.batchCreate.sourceNodeName};
            $.ajax({
                data: data,
                type: "POST",
                url: contextRoot + "batchCreateSameNode",
                success: function (result) {
                    if (result.code == 200) {
                        _this.isBatchCreate = false;
                        let newNodes = result.data;
                        newNodes.forEach(function (m) {
                            let sobj = _this.graph.nodes.find(function (x) {
                                return x.uuid === m.uuid
                            })
                            if (typeof(sobj) == "undefined") {
                                _this.graph.nodes.push(m);
                            }
                        })
                        _this.updateGraph();
                        _this.batchCreate.sourceNodeName = '';
                        _this.$message({
                            message: '操作成功',
                            type: 'success'
                        });
                    }
                }
            });
        }
    }
})
$(function () {
    $(".blankmenubar").click(function () {
        $('#blank_menubar').hide();
    })
    $(".blankmenubar").mouseleave(function () {
        $('#blank_menubar').hide();
    })
    $(".graphContainer").bind("contextmenu", function (event) {
        app.svg.selectAll(".buttongroup").classed("circle_operate", true);
        let left = event.clientX;
        let top = event.clientY;
        document.getElementById('blank_menubar').style.position = 'absolute';
        document.getElementById('blank_menubar').style.left = left + 'px';
        document.getElementById('blank_menubar').style.top = top + 'px';
        $('#blank_menubar').show();
        event.preventDefault();
    });
    $(".graphContainer").bind("click", function (event) {
    	if (event.target.tagName!="circle"&&event.target.tagName!="link") {
        	d3.select('#nodeDetail').style('display', 'none');
        }
        if (!(event.target.id === "jsoncontainer" || $(event.target).parents("#jsoncontainer").length > 0)) {
            app.jsonShow=false;
        }
        let cursor=document.getElementById("graphContainer").style.cursor;
        if(cursor=='crosshair'){
            d3.select('.graphContainer').style("cursor", "");
            app.txx=event.offsetX;
            app.tyy=event.offsetY;
            app.createSingleNode();
        }
        event.preventDefault();
    });

    $(".linkmenubar").bind("mouseleave", function (event) {
        d3.select('#link_menubar').style('display', 'none');
    });
    $("body").bind("mousedown", function (event) {
        if (!(event.target.id === "linkmenubar" || $(event.target).parents("#linkmenubar").length > 0)) {
            $("#linkmenubar").hide();
        }
        if (!(event.target.id === "batchcreateform" || $(event.target).parents("#batchcreateform").length > 0)) {
            app.isBatchCreate = false;
        }
        if (!(event.target.id === "richContainer" || $(event.target).parents("#richContainer").length > 0)) {
            $("#richContainer").hide();
        }
        if (!(event.target.id === "nodeDetail" || $(event.target).parents("#nodeDetail").length > 0)) {
        	d3.select('#nodeDetail').style('display', 'none');
        }
        
    });
})
	 
	 